def ftoc(ftemp)
  (5.0 / 9.0) * (ftemp - 32)
end

def ctof(ctemp)
  (9.0 / 5.0) * ctemp + 32
end
