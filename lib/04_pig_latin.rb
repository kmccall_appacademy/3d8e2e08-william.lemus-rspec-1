def translate(string)
  array_of_words = string.split
  array_of_words.map do |word|
    if vowel?(word)
      word + 'ay'
    else
      vowel_idx = next_vowel(word)
      pig_latinize(word, vowel_idx) unless vowel_idx.nil?
    end
  end.join(' ')

end

VOWELS = %(a e i o u)
def vowel?(string)
  VOWELS.include?(string[0].downcase)
end

def next_vowel(string)
  string.each_char.with_index do |ch, idx|
    if vowel?(ch)
      return idx - 1 if string[idx - 1] == 'q'
      return idx
    end
  end
  nil
end

def pig_latinize(word, vowel_idx)
  if word[vowel_idx] == 'q'
    word[vowel_idx + 2..-1] + word[0...vowel_idx + 2] + 'ay'
  else
    word[vowel_idx..-1] + word[0...vowel_idx] + 'ay'
  end
end
