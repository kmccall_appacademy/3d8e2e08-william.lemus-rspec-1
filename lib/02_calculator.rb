def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  return arr.reduce(:+) unless arr.length.zero?
  0
end
