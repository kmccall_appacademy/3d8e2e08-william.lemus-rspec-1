def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, repetitions = 2)
  repeated = string
  (repetitions - 1).times do
    repeated += ' ' + string
  end
  repeated
end

def start_of_word(string, num)
  string[0...num]
end

def first_word(arr)
  arr.split[0]
end

def titleize(string)
  arr = string.split
  arr[0] = arr[0].capitalize
  return arr[0] if arr.length == 1
  arr[0] + ' ' + capitalize(arr)
end

def capitalize(arr)
  exceptions = 'andoverthen'
  arr[1..-1].map do |word|
    if exceptions.include?(word)
      word
    else
      word.capitalize
    end
  end.join(' ')
end
